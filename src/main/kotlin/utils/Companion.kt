package utils

import objects.User

class Companion {
    companion object CompanionObj{
        val stages : ArrayList<Pair<Int, String>> by lazy{ arrayListOf(
            Pair(1, "Backlog"),
            Pair(2, "Pendiente"),
            Pair(3, "En progreso"),
            Pair(4, "QA"),
            Pair(5, "Finalizado")
        ) }

        val roles by lazy { arrayListOf(
                Pair(1, "Administrador"),
                Pair(2, "Operador"),
                Pair(3, "Tecnico")
        ) }

        var currentUser : User? = null
    }
}