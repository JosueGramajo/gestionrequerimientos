package utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils{
    fun changeDateFormat(inputFormatStr : String, outputFormatStr : String, dateStr : String) : String{
        val inputFormat = SimpleDateFormat(inputFormatStr)
        val date= inputFormat.parse(dateStr)
        val outputFormat = SimpleDateFormat(outputFormatStr)
        return outputFormat.format(date)
    }
    fun parseDateWithoutTime(date : String) : Date {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        return sdf.parse(date)
    }
}