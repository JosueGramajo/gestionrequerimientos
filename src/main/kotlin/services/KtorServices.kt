package services

import com.fasterxml.jackson.databind.ObjectMapper
import firestore.FirestoreUtils
import handlers.IssueHandler
import handlers.ProjectHandler
import handlers.UserHandler
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.jackson
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import objects.Project
import objects.User
import utils.Companion
import java.text.DateFormat


// Entry Point of the application as defined in resources/application.conf.
// @see https://ktor.io/servers/configuration.html#hocon-file
fun Application.main() {
    // This adds Date and Server headers to each response, and allows custom additional headers
    install(DefaultHeaders)
    // This uses use the logger to log every call (request/response)
    install(CallLogging)

    install(ContentNegotiation) {
        jackson {
            dateFormat = DateFormat.getDateInstance()
            disableDefaultTyping()
        }
    }

    routing {
        get("/") {
            call.respondRedirect("/login")
        }

        post("/doLogin"){
            val params = call.receiveParameters()
            val email = params["email"] ?: ""
            val password = params["password"] ?: ""

            FirestoreUtils.authenticateUser(email, password)?.let{
                Companion.currentUser = it

                call.response.status(HttpStatusCode.OK)
                call.respondText { "Login exitoso" }
            } ?: run {
                call.response.status(HttpStatusCode.Unauthorized)
                call.respondText { "Usuario o constraseña incorrecta" }
            }
        }

        get("/doLogout"){
            Companion.currentUser = null
            call.respondRedirect ("/login")
        }

        //USER OPERATIONS
        post("/insertUser") {
            val params = call.receiveParameters()
            val email = params["email"] ?: ""
            val password = params["password"] ?: ""
            val name = params["name"] ?: ""
            val role = params["role"] ?: "0"
            val code = params["code"] ?: ""
            val department = params["department"] ?: ""

            UserHandler.addUser(email, password, name, role.toInt(), code, department)

            call.respondText { "User created successfully" }
        }
        delete("/deleteUser") {
            FirestoreUtils.deleteDocumentWithId(FirestoreUtils.USER_COLLECTION, call.receiveParameters()["id"] ?: "")
            call.respondText { "Documento eliminado exitosamente" }
        }
        put("/updateUser") {
            val params = call.receiveParameters()
            val id = params["id"] ?: ""
            val email = params["email"] ?: ""
            val name = params["name"] ?: ""
            val role = params["role"] ?: "0"
            val code = params["code"] ?: ""
            val department = params["department"] ?: ""
            UserHandler.updateUser(id, email, name, role.toInt(), code, department)
            call.respondText { "Update exitoso" }
        }
        post("/getUserWithId") {
            val id = call.receiveParameters()["id"] ?: ""
            val user = FirestoreUtils.getObject<User>(FirestoreUtils.USER_COLLECTION, id)
            call.respondText { ObjectMapper().writeValueAsString(user) }
        }
        get("/getUser") {
            val email = call.request.queryParameters["email"] ?: ""
            val resultUser = FirestoreUtils.getUser(email)
            resultUser ?: kotlin.run {
                call.respondText { "Usuario no encontrado" }
                return@get
            }
            call.respondText { "Usuario encontrado: ${resultUser.email} - ${resultUser.password} - ${resultUser.name} - ${resultUser.role}" }
        }

        //ISSUE OPERATIONS
        post("/insertIssue") {
            val params = call.receiveParameters()
            val title = params["title"] ?: ""
            val description = params["description"] ?: ""
            val projectId = params["projectId"] ?: ""
            val startDate = params["startDate"] ?: ""
            val endDate = params["endDate"] ?: ""
            val category = params["category"] ?: ""
            val technician = params["technician"] ?: ""
            val technicianId = params["technicianId"] ?: ""
            val tasks = params["tasks"] ?: ""
            val stage = params["stage"] ?: "0"

            IssueHandler.insertIssue(title, description, projectId, startDate, endDate, "", category, technician, technicianId, tasks, stage.toInt())

            call.respondText { "User created successfully" }
        }
        post("/getIssue"){
            val id = call.receiveParameters()["id"] ?: ""

            val issue = FirestoreUtils.getIssue(id)

            issue?.let {
                call.respondText { ObjectMapper().writeValueAsString(it) }
            } ?: kotlin.run {
                call.response.status(HttpStatusCode.Unauthorized)
                call.respondText { "No se encontro el issue" }
            }
        }
        post("/updateIssueStage"){
            val params = call.receiveParameters()
            val issueId = params["issueId"] ?: ""
            val state = params["state"] ?: "0"

            IssueHandler.updateIssueStage(issueId, state.toInt())

            call.respondText { "Update exitoso" }
        }

        //TASK OPERATIONS
        post("/updateTaskState"){
            val params = call.receiveParameters()
            val issueId = params["issueId"] ?: ""
            val taskId = params["taskId"] ?: ""
            val completed = params["completed"] ?: "false"

            FirestoreUtils.updateIssueTaskState(issueId, taskId, completed.toBoolean())

            call.response.status(HttpStatusCode.OK)
            call.respondText { "Update exitoso" }
        }

        //PROJECT OPERATIONS
        post("/addProject") {
            val params = call.receiveParameters()
            val name = params["name"] ?: ""
            val description = params["description"] ?: ""
            val personInCharge = params["personInCharge"] ?: ""
            val startDate = params["startDate"] ?: ""
            val estimatedEndDate = params["estimatedEndDate"] ?: ""
            val cost = params["cost"] ?: "0"

            ProjectHandler.insertProject(name, description, startDate, estimatedEndDate, cost.toDouble(), personInCharge)

            call.respondText { "Operacion exitosa" }
        }
        delete("/deleteProject"){
            val id = call.receiveParameters()["id"] ?: ""
            FirestoreUtils.deleteDocumentWithId(FirestoreUtils.PROJECT_COLLECTION, id)
            call.respondText { "Operacion exitosa" }
        }
        put("/updateProject") {
            val params = call.receiveParameters()
            val id = params["id"] ?: ""
            val name = params["name"] ?: ""
            val description = params["description"] ?: ""
            val personInCharge = params["personInCharge"] ?: ""
            val startDate = params["startDate"] ?: ""
            val estimatedEndDate = params["estimatedEndDate"] ?: ""
            val cost = params["cost"] ?: "0"

            ProjectHandler.updateProject(id, name, description, startDate, estimatedEndDate, cost.toDouble(), personInCharge)

            call.respondText { "Operacion exitosa" }
        }
        post("/getProjectWithId"){
            val id = call.receiveParameters()["id"] ?: ""
            val project = FirestoreUtils.getObject<Project>(FirestoreUtils.PROJECT_COLLECTION, id)
            call.respondText { ObjectMapper().writeValueAsString(project) }
        }
    }
}