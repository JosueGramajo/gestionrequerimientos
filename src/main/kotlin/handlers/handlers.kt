package handlers

import firestore.FirestoreUtils
import objects.*
import utils.Companion
import utils.DateUtils
import java.lang.Math.ceil
import java.util.*

object UserHandler {
    fun addUser(email : String, password : String, name : String, role : Int, code : String, department : String){
        val newUser = User(email, password, name, role, "", code, department, "", "")

        FirestoreUtils.insertObjectWithRandomDocumentID(FirestoreUtils.USER_COLLECTION, newUser)
    }

    fun updateUser(userId : String, email : String, name : String, role : Int, code : String, department : String){
        val password = FirestoreUtils.getUserPassword(userId)

        val newUser = User(email, password, name, role, "", code, department, "", "")

        FirestoreUtils.updateDocumentWithObject(FirestoreUtils.USER_COLLECTION, userId, newUser)
    }
}

object ProjectHandler{
    fun insertProject(name: String,
                      description: String,
                      startDate: String,
                      estimatedEndDate: String,
                      cost: Double,
                      personInCharge: String){

        val project = Project(name, description, 0.00, startDate, estimatedEndDate, "", cost, personInCharge, "")

        FirestoreUtils.insertObjectWithRandomDocumentID(FirestoreUtils.PROJECT_COLLECTION, project)
    }

    fun updateProject(id: String,
                      name: String,
                      description: String,
                      startDate: String,
                      estimatedEndDate: String,
                      cost: Double,
                      personInCharge: String){

        val existingProj = FirestoreUtils.getObject<Project>(FirestoreUtils.PROJECT_COLLECTION, id) ?: Project()

        val project = Project(name, description, existingProj.percentageFinished, startDate, estimatedEndDate, existingProj.endDate, cost, personInCharge, "")

        FirestoreUtils.updateDocumentWithObject(FirestoreUtils.PROJECT_COLLECTION, id, project)
    }

    fun getProjectWithParsedData(id : String) : ProjectParsedData?{
        FirestoreUtils.getObject<Project>(FirestoreUtils.PROJECT_COLLECTION, id)?.let{ project ->
            val response = ProjectParsedData()
            response.baseProject = project

            val parsed = DateUtils.changeDateFormat("dd/MM/yyyy","yyyy-MM-dd", project.estimatedEndDate)
            val endDate = DateUtils.parseDateWithoutTime(parsed)
            val diff = endDate.time - Date().time
            response.remainingTime = (diff/1000/60/60/24).toInt()

            val projectIssues = FirestoreUtils.getIssues(id)
            val completed = projectIssues.count { it.stage == "Finalizado" }
            response.completedTasks = "$completed/${projectIssues.count()}"

            response.baseProject.percentageFinished = ceil(((completed * 100) / projectIssues.count()).toDouble())

            return response
        }

        return null
    }
}

object IssueHandler{
    fun insertIssue(title : String,
                    description: String,
                    projectId : String,
                    startDate : String,
                    estimatedEndDate : String,
                    endDate : String,
                    category : String,
                    technician : String,
                    technicianId: String,
                    tasks : String,
                    stage : Int){

        val number = FirestoreUtils.getLatestIssueNumber()

        val tasksList = arrayListOf<Task>()
        tasks.split("|").map {
            if(it.isNotEmpty()){
                tasksList.add(Task(UUID.randomUUID().toString(), it, false))
            }
        }

        val stage = Companion.stages.firstOrNull { it.first == stage }?.second ?: "Backlog"

        val issue = Issue(number + 1, title, description, projectId, startDate, estimatedEndDate, endDate, category, stage, technician, technicianId, tasksList, "", "")

        FirestoreUtils.insertObjectWithRandomDocumentID(FirestoreUtils.ISSUE_COLLECTION, issue)
    }

    fun updateIssueStage(issueId : String, state : Int){
        val stage = Companion.stages.firstOrNull { it.first == state }?.second ?: "Backlog"

        FirestoreUtils.updateIssueStage(issueId, stage)
    }

    fun getIssuesWithParsedInfo(projectId : String) : List<IssueParsedData>{
        val issues = FirestoreUtils.getIssues(projectId)

        val responseList = arrayListOf<IssueParsedData>()
        issues.map {
            val parsedIssue = IssueParsedData()
            parsedIssue.baseIssue = it
            parsedIssue.completedTasks = "${it.tasks.count { it.completed }}/${it.tasks.count()}"
            parsedIssue.percentageCompleted = ceil((it.tasks.count { it.completed } * 100).toDouble() / it.tasks.count().toDouble()).toInt()

            val parsed = DateUtils.changeDateFormat("dd/MM/yyyy","yyyy-MM-dd", it.estimatedEndDate)
            val endDate = DateUtils.parseDateWithoutTime(parsed)

            val diff = endDate.time - Date().time

            parsedIssue.remainingTime = (diff/1000/60/60/24).toInt()

            responseList.add(parsedIssue)
        }

        return responseList
    }
}

object StatisticsHandler{
    fun getDashboardStatistics() : Statistics{
        val issues = FirestoreUtils.getObjectList<Issue>(FirestoreUtils.ISSUE_COLLECTION)
        val userList = FirestoreUtils.getUsers()

        val issueCount = issues.count()
        val resolvedIssuesCount = issues.count { it.stage == "Finalizado" }
        val users = FirestoreUtils.getObjectList<User>(FirestoreUtils.USER_COLLECTION).count()
        val projects = FirestoreUtils.getObjectList<Project>(FirestoreUtils.PROJECT_COLLECTION).count()

        val userIssues = arrayListOf<Statistics.UserIssue>()
        val sortedUsers = issues.distinctBy { it.technicianId }.map { it.technicianId }
        sortedUsers.map { user ->
            val totalIssues = issues.count { it.technicianId == user }
            val backlog = issues.count { it.technicianId == user && it.stage == "Backlog" }
            val pending = issues.count { it.technicianId == user && it.stage == "Pendiente" }
            val inProgress = issues.count { it.technicianId == user && it.stage == "En progreso" }
            val qa = issues.count { it.technicianId == user && it.stage == "QA" }
            val resolved = issues.count { it.technicianId == user && it.stage == "Finalizado" }

            userIssues.add(Statistics.UserIssue(userList.firstOrNull { it.id == user }?.name ?: "", totalIssues, backlog, pending, inProgress, qa, resolved))
        }

        return Statistics(users, projects, issueCount, resolvedIssuesCount, userIssues)
    }
}