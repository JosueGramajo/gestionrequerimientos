package firestore

import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.Query
import com.google.cloud.firestore.QueryDocumentSnapshot
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.cloud.FirestoreClient
import objects.Issue
import objects.Project
import objects.User
import utils.Companion
import java.io.FileInputStream


object FirestoreUtils {
    //gestionrequerimientos-d0480-6daae4c6546e.json
    val USER_COLLECTION = "users"
    val PROJECT_COLLECTION = "projects"
    val ISSUE_COLLECTION = "issues"
    val ROLE_COLLECTION = "roles"
    val CATALOG_COLLECTION = "catalogs"

    val DEPARTMENTS_DOCUMENT = "departments"
    val ISSUE_CAT_DOCUMENT = "issue_categories"

    //val local = "src/main/webapp/"
    const val local = ""

    //GENERAL METHODS
    fun initFirestore() : Firestore {
        val input = FileInputStream("${local}WEB-INF/gestionrequerimientos-d0480-6daae4c6546e.json")
        val credentials = GoogleCredentials.fromStream(input)
        val options = FirebaseOptions.Builder()
            .setCredentials(credentials)
            .build()

        if (FirebaseApp.getApps().isEmpty()){
            FirebaseApp.initializeApp(options)
        }

        return FirestoreClient.getFirestore()
    }

    fun insertObjectWithRandomDocumentID(collection : String, obj : Any){
        val db = initFirestore()
        val future = db.collection(collection).document().set(obj)
        println(future.get().updateTime)
    }

    fun deleteDocumentWithId(collection: String, documentID : String){
        val db = initFirestore()
        db.collection(collection).document(documentID).delete()
    }

    fun getCatalog(document : String) : List<String>{
        val db = initFirestore()
        val categories = db.collection(CATALOG_COLLECTION).document(document)
        val res = categories.get().get()
        return res["catalog"] as List<String>
    }

    inline fun <reified T : Any> getObjectList(collection  : String) : List<T>{
        val db = initFirestore()
        val future = db.collection(collection).get()
        val documents = future.get().documents
        val responseList = arrayListOf<T>()
        documents.map { responseList.add(it.toObject(T::class.java)) }
        return responseList
    }

    inline fun <reified T : Any> getObject(collectionId  : String, documentId: String) : T?{
        val db = initFirestore()
        val document = db.collection(collectionId).document(documentId).get().get()
        return document.toObject(T::class.java)
    }

    fun updateDocumentWithObject(collection: String, documentId: String, obj : Any){
        val db = initFirestore()
        db.collection(collection).document(documentId).set(obj)
    }

    //CASE SPECIFIC METHODS
    fun getUser(email : String) : User?{
        val db = initFirestore()
        val cities = db.collection(USER_COLLECTION)
        val query = cities.whereEqualTo("email", email)
        val querySnapshot = query.get()

        querySnapshot.get().documents.map {
            return it.toObject(User::class.java)
        }

        return null
    }

    fun authenticateUser(email : String, password : String) : User?{
        val db = initFirestore()
        val users = db.collection(USER_COLLECTION)
        val query = users.whereEqualTo("email", email).whereEqualTo("password", password)
        val querySnapshot = query.get()
        querySnapshot.get().documents.map {
            val user = it.toObject(User::class.java)
            user.id = it.id
            return user
        }
        return null
    }

    fun getLatestIssueNumber() : Int{
        val db = initFirestore()
        val issues = db.collection(ISSUE_COLLECTION)
        val query = issues.orderBy("number", Query.Direction.DESCENDING).limit(1)
        val querySnapshot = query.get()

        if (querySnapshot.get().documents.isNotEmpty()){
            return querySnapshot.get().documents.first()["number"].toString().toInt()
        }

        return 0
    }

    fun getProjectList() : List<Project>{
        val db = initFirestore()
        val future = db.collection(PROJECT_COLLECTION).get()
        val documents = future.get().documents
        val responseList = arrayListOf<Project>()
        documents.map {
            val proj = it.toObject(Project::class.java)
            proj.id = it.id
            responseList.add(proj)
        }
        return responseList
    }

    fun getIssues(projectId : String) : List<Issue>{
        val db = initFirestore()
        val future = db.collection(ISSUE_COLLECTION).whereEqualTo("projectId", projectId).get()
        val documents = future.get().documents
        val responseList = arrayListOf<Issue>()
        documents.map {
            val issue = it.toObject(Issue::class.java)
            issue.id = it.id
            val s = issue.technician.split(" ")
            if (s.size >= 2){
                issue.image = s[0].substring(0,1).toLowerCase() + s[1].substring(0,1).toLowerCase()
            }

            responseList.add(issue)
        }
        return responseList
    }

    fun getIssuesFromSpecifiTechnician(projectId : String, technicianId : String) : List<Issue>{
        val db = initFirestore()
        val future = db.collection(ISSUE_COLLECTION)
                .whereEqualTo("projectId", projectId)
                .whereEqualTo("technicianId", technicianId)
                .get()

        val documents = future.get().documents
        val responseList = arrayListOf<Issue>()
        documents.map {
            val issue = it.toObject(Issue::class.java)
            issue.id = it.id
            val s = issue.technician.split(" ")
            if (s.size >= 2){
                issue.image = s[0].substring(0,1).toLowerCase() + s[1].substring(0,1).toLowerCase()
            }

            responseList.add(issue)
        }
        return responseList
    }

    fun getTechnicians() : List<User>{
        val db = initFirestore()
        val future = db.collection(USER_COLLECTION).whereEqualTo("role", 3).get()
        val documents = future.get().documents
        val responseList = arrayListOf<User>()
        documents.map {
            val user = it.toObject(User::class.java)
            user.id = it.id
            responseList.add(user)
        }
        return responseList
    }

    fun getIssue(id : String) : Issue?{
        val db = initFirestore()
        val future = db.collection(ISSUE_COLLECTION).document(id).get()
        val document = future.get()
        val issue = document.toObject(Issue::class.java)
        issue?.let {
            issue.id = document.id

            val s = it.technician.split(" ")
            issue.image = s[0].substring(0,1).toLowerCase() + s[1].substring(0,1).toLowerCase()
        }
        return issue
    }
    fun updateIssueTaskState(issueId : String, taskId : String, completed : Boolean){
        val db = initFirestore()
        val docRef = db.collection(ISSUE_COLLECTION).document(issueId)

        docRef.get().get().toObject(Issue::class.java)?.let { existingObj ->
            existingObj.tasks.map { task ->
                if (task.id.equals(taskId)){
                    task.completed = completed

                    docRef.update("tasks", existingObj.tasks)

                    return@let
                }
            }
        }
    }

    fun updateIssueStage(issueId : String, stage : String){
        val db = initFirestore()
        val docRef = db.collection(ISSUE_COLLECTION).document(issueId)
        docRef.update("stage", stage)
    }

    fun getUsers() : List<User>{
        val db = initFirestore()
        val future = db.collection(USER_COLLECTION).get()
        val documents = future.get().documents
        val responseList = arrayListOf<User>()
        documents.map {
            val user = it.toObject(User::class.java)
            user.id = it.id
            user.roleName = Companion.roles.firstOrNull { it.first == user.role }?.second ?: ""
            responseList.add(user)
        }
        return responseList
    }

    fun getUserPassword(id : String) : String{
        val db = initFirestore()
        val user = db.collection(USER_COLLECTION).document(id).get()
        user.get()["password"]?.let {
            return it.toString()
        }
        return ""
    }
}