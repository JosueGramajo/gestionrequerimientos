package servlets

import firestore.FirestoreUtils
import handlers.IssueHandler
import handlers.ProjectHandler
import handlers.StatisticsHandler
import objects.Issue
import objects.Project
import objects.ProjectParsedData
import objects.Role
import utils.Companion
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class LoginServlet : HttpServlet(){
    override fun doGet(req: HttpServletRequest?, resp: HttpServletResponse?) {
        req!!.getRequestDispatcher("/frontend/login.jsp").forward(req, resp)
    }
}

class IndexServlet : HttpServlet(){
    override fun doGet(req: HttpServletRequest?, resp: HttpServletResponse?) {
        Companion.currentUser?.let { user ->
            req!!.setAttribute("role", user.role)
            req.getRequestDispatcher("/frontend/index.jsp").forward(req, resp)
        } ?: kotlin.run {
            req!!.getRequestDispatcher("/login").forward(req, resp)
        }
    }
}

class ProjectSelectorServlet : HttpServlet(){
    override fun doGet(req: HttpServletRequest?, resp: HttpServletResponse?) {
        Companion.currentUser?.let { user ->
            val flow = req!!.getParameter("flow")

            val projects = FirestoreUtils.getProjectList()

            req.setAttribute("projects", projects)
            req.setAttribute("flow", flow)
            req.setAttribute("role", user.role)
            req.getRequestDispatcher("/frontend/project-selector.jsp").forward(req, resp)
        } ?: kotlin.run {
            req!!.getRequestDispatcher("/login").forward(req, resp)
        }
    }
}

class KanbanBoardServlet : HttpServlet(){
    override fun doGet(req: HttpServletRequest?, resp: HttpServletResponse?) {
        Companion.currentUser?.let { user ->
            var projectId = ""
            var flow = ""
            try {
                projectId = req!!.getParameter("project")
                flow = req.getParameter("flow")
            }catch (ex : Exception){
                req!!.getRequestDispatcher("/frontend/project-selector.jsp").forward(req, resp)
            }

            if (flow.equals("1")){
                val project = ProjectHandler.getProjectWithParsedData(projectId) ?: ProjectParsedData()
                val issues = IssueHandler.getIssuesWithParsedInfo(projectId)

                req!!.setAttribute("role", user.role)
                req.setAttribute("project", project)
                req.setAttribute("issues", issues)
                req.getRequestDispatcher("/frontend/project-detail.jsp").forward(req, resp)
            }else{
                val issues: List<Issue> = if (user.role == 3){
                    FirestoreUtils.getIssuesFromSpecifiTechnician(projectId, user.id)
                }else{
                    FirestoreUtils.getIssues(projectId)
                }

                val categories = FirestoreUtils.getCatalog(FirestoreUtils.ISSUE_CAT_DOCUMENT)
                val technicians = FirestoreUtils.getTechnicians()
                val project = FirestoreUtils.getObject(FirestoreUtils.PROJECT_COLLECTION, projectId) ?: Project()

                req!!.setAttribute("issues", issues)
                req.setAttribute("categories", categories)
                req.setAttribute("technicians", technicians)
                req.setAttribute("project", project)
                req.setAttribute("projectId", projectId)
                req.setAttribute("flow", flow)
                req.setAttribute("role", user.role)
                req.getRequestDispatcher("/frontend/kanban-board.jsp").forward(req, resp)
            }
        } ?: kotlin.run {
            req!!.getRequestDispatcher("/login").forward(req, resp)
        }
    }
}

class ErrorServlet : HttpServlet(){
    override fun doGet(req: HttpServletRequest?, resp: HttpServletResponse?) {
        req!!.getRequestDispatcher("/frontend/error-page.jsp").forward(req, resp)
    }
}

class UserManagementServlet : HttpServlet(){
    override fun doGet(req: HttpServletRequest?, resp: HttpServletResponse?) {
        Companion.currentUser?.let { user ->
            if (user.role == 1){
                val users = FirestoreUtils.getUsers()
                val roles = FirestoreUtils.getObjectList<Role>(FirestoreUtils.ROLE_COLLECTION)
                val departments = FirestoreUtils.getCatalog(FirestoreUtils.DEPARTMENTS_DOCUMENT)

                req!!.setAttribute("users", users)
                req.setAttribute("role", user.role)
                req.setAttribute("departments", departments)
                req.setAttribute("roles", roles)
                req.getRequestDispatcher("/frontend/user-management.jsp").forward(req, resp)
            }else{
                req!!.getRequestDispatcher("/error").forward(req, resp)
            }
        } ?: run{
            req!!.getRequestDispatcher("/login").forward(req, resp)
        }
    }
}

class ProjectManagementServlet : HttpServlet(){
    override fun doGet(req: HttpServletRequest?, resp: HttpServletResponse?) {
        Companion.currentUser?.let { user ->
            if (user.role == 1){
                val users = FirestoreUtils.getUsers()
                val projects = FirestoreUtils.getProjectList()

                req!!.setAttribute("users", users)
                req.setAttribute("projects", projects)
                req.setAttribute("role", user.role)

                req.getRequestDispatcher("/frontend/project-management.jsp").forward(req, resp)
            }else{
                req!!.getRequestDispatcher("/error").forward(req, resp)
            }
        } ?: run{
            req!!.getRequestDispatcher("/login").forward(req, resp)
        }
    }
}

class DashboardServlet : HttpServlet(){
    override fun doGet(req: HttpServletRequest?, resp: HttpServletResponse?) {
        Companion.currentUser?.let { user ->
            if (user.role == 1){
                val stats = StatisticsHandler.getDashboardStatistics()

                req!!.setAttribute("role", user.role)
                req.setAttribute("stats", stats)
                req.getRequestDispatcher("/frontend/dashboard.jsp").forward(req, resp)
            }else{
                req!!.getRequestDispatcher("/error").forward(req, resp)
            }
        } ?: run{
            req!!.getRequestDispatcher("/login").forward(req, resp)
        }
    }
}

