<%--
  Created by IntelliJ IDEA.
  User: josue
  Date: 26/05/2021
  Time: 18:38
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: josue
  Date: 20/05/2021
  Time: 23:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script type="text/javascript" src="https://gc.kis.v2.scr.kaspersky-labs.com/FD126C42-EBFA-4E12-B309-BB3FDD723AC1/main.js?attr=svWUvNp_wfq9_PMkUoePvVayIiFfYR8GPjEPBoqk9QLreEsa39wJ_n1LqTG7mhpq4486VaJcPCtquP3FMRUAz09Vo7WhQ2OCWXUGHa3DGUE" charset="UTF-8"></script><script async src="https://www.googletagmanager.com/gtag/js?id=UA-52115242-14"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-52115242-14');
    </script>
    <meta charset="utf-8">
    <title>Pipeline Project Management Bootstrap Theme</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A project management Bootstrap theme by Medium Rare">
    <link href="../assets/img/favicon.ico" rel="icon" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gothic+A1" rel="stylesheet">
    <link href="../assets/css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link href="../assets/plugins/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Required vendor scripts (Do not remove) -->
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.js"></script>

    <link href="../assets/plugins/air-datepicker/css/datepicker.min.css" rel="stylesheet">
    <script src="../assets/plugins/air-datepicker/js/datepicker.min.js"></script>
    <script src="../assets/plugins/air-datepicker/js/i18n/datepicker.en.js"></script>
    <script src="../assets/plugins/air-datepicker/js/i18n/datepicker.es.js"></script>
</head>

<body>

<div class="layout layout-nav-side">
    <div class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top">

        <a class="navbar-brand" href="/homePage">
            <img alt="Pipeline" src="../assets/img/umg.png" width="50px" height="50px" />
        </a>
        <div class="d-flex align-items-center">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="d-block d-lg-none ml-2">
                <div class="dropdown">
                    <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img alt="Image" src="../assets/img/user.png" class="avatar" />
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="/doLogout" class="dropdown-item">Cerrar sesión</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="collapse navbar-collapse flex-column" id="navbar-collapse">
            <ul class="navbar-nav d-lg-block">

                <li class="nav-item">
                    <a class="nav-link" href="/homePage">Inicio</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/projectSelector?flow=1">Proyectos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/projectSelector?flow=2">Incidencias</a>
                </li>

            </ul>

            <c:if test="${role eq 1}">
                <hr>
                <div class="d-none d-lg-block w-100">
                    <span class="text-small text-muted">Opciones administrador</span>
                    <ul class="nav nav-small flex-column mt-2">
                        <li class="nav-item">
                            <a href="/dashboard" class="nav-link">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a href="/userManagement" class="nav-link">Mantenimiento usuarios</a>
                        </li>
                        <li class="nav-item">
                            <a href="/projectManagement" class="nav-link">Mantenimiento proyectos</a>
                        </li>
                        
                        <li class="nav-item">

                        </li>
                    </ul>
                    <hr>
                </div>
            </c:if>

        </div>
        <div class="d-none d-lg-block">
            <div class="dropup">
                <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img alt="Image" src="../assets/img/user.png" class="avatar" />
                </a>
                <div class="dropdown-menu">
                    <a href="/doLogout" class="dropdown-item">Cerrar sesión</a>
                </div>
            </div>
        </div>
    </div>

    <div class="main-container">
        <div class="navbar bg-white breadcrumb-bar">
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-11 col-xl-10">
                    <div class="page-header">
                        <h3>Manejo de proyectos</h3>
                    </div>
                    <div>
                        <button class="btn btn-success" id="addProjectButton">Agregar proyecto</button>
                    </div>
                    <br><hr>
                    <div class="table-responsive table-responsive-data2">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Responsable</th>
                                <th>Porcentaje finalizado</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="item" items="${projects}">
                                <tr>
                                    <td>${item.name}</td>
                                    <td>${item.description}</td>
                                    <td>${item.personInCharge}</td>
                                    <td>${item.percentageFinished}%</td>
                                    <td>
                                        <div class="table-data-feature">
                                            <button style="color: #4EB5E6;" class="item edit-button" data-id="${item.id}" data-toggle="tooltip" data-placement="top" title="Editar">
                                                <i class="zmdi zmdi-edit"></i>
                                            </button>
                                            <button style="color: #c82333;" class="item delete-button" data-id="${item.id}" data-toggle="tooltip" data-placement="top" title="Delete">
                                                <i class="zmdi zmdi-delete"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<div id="confirmationModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label id="confirmationText"></label>
            </div>
            <div class="modal-footer">
                <button type="button" id="confirmationAccept" class="btn btn-primary">Aceptar</button>
                <button type="button" id="confirmationCancel" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div id="infoModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Información</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label id="infoModalMessage"></label>
            </div>
            <div class="modal-footer">
                <button id="infoAccept" type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="insertProjectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insertar proyecto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="projectName" class="col-form-label">Nombre:</label>
                        <input type="text" class="form-control" id="projectName">
                    </div>
                    <div class="form-group">
                        <label for="projectDescription" class="col-form-label">Descripcion:</label>
                        <input type="text" class="form-control" id="projectDescription">
                    </div>
                    <div class="form-group">
                        <label for="projectPerson" class="col-form-label">Persona a cargo:</label>
                        <select class="custom-select" id="projectPerson">
                            <option value="-1" selected>Seleccione uno...</option>

                            <c:forEach var="item" items="${users}">
                                <option value="${item.name}">${item.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="projectCost" class="col-form-label">Costo:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Q</span>
                            </div>
                            <input type="text" class="form-control" id="projectCost" aria-describedby="basic-addon1">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <div class="search-container">
                                    <label for="startDate" class="control-label mb-1">Fecha inicio</label>
                                    <input type='text' class="form-control" id="startDate" />
                                </div>
                            </div>
                            <div class="col">
                                <div class="search-container">
                                    <label for="endDate" class="control-label mb-1">Fecha estimada fin</label>
                                    <input type='text' class="form-control" id="endDate" />
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="saveProjectButton" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Optional Vendor Scripts (Remove the plugin script here and comment initializer script out of index.js if site does not use that feature) -->

<!-- Autosize - resizes textarea inputs as user types -->
<script type="text/javascript" src="../assets/js/autosize.min.js"></script>
<!-- Flatpickr (calendar/date/time picker UI) -->
<script type="text/javascript" src="../assets/js/flatpickr.min.js"></script>
<!-- Prism - displays formatted code boxes -->
<script type="text/javascript" src="../assets/js/prism.js"></script>
<!-- Shopify Draggable - drag, drop and sort items on page -->
<script type="text/javascript" src="../assets/js/draggable.bundle.legacy.js"></script>
<script type="text/javascript" src="../assets/js/swap-animation.js"></script>
<!-- Dropzone - drag and drop files onto the page for uploading -->
<script type="text/javascript" src="../assets/js/dropzone.min.js"></script>
<!-- List.js - filter list elements -->
<script type="text/javascript" src="../assets/js/list.min.js"></script>

<!-- Required theme scripts (Do not remove) -->
<script type="text/javascript" src="../assets/js/theme.js"></script>

<!-- DataTables  & Plugins -->
<script src="../assets/plugins/datatables/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/plugins/datatables/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../assets/plugins/datatables/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../assets/plugins/datatables/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../assets/plugins/datatables/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../assets/plugins/datatables/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../assets/plugins/datatables/jszip/jszip.min.js"></script>
<script src="../assets/plugins/datatables/pdfmake/pdfmake.min.js"></script>
<script src="../assets/plugins/datatables/pdfmake/vfs_fonts.js"></script>
<script src="../assets/plugins/datatables/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../assets/plugins/datatables/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../assets/plugins/datatables/datatables-buttons/js/buttons.colVis.min.js"></script>

<script src="../assets/js/project-management.js"></script>

<script>
    $(function () {
        $("#example1").DataTable({
            "responsive": true, "lengthChange": false, "autoWidth": false,
            "buttons": ["copy", "excel", "pdf", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });
</script>

<!-- This appears in the demo only - demonstrates different layouts -->
<style type="text/css">
    .layout-switcher{ position: fixed; bottom: 0; left: 50%; transform: translateX(-50%) translateY(73px); color: #fff; transition: all .35s ease; background: #343a40; border-radius: .25rem .25rem 0 0; padding: .75rem; z-index: 999; }
    .layout-switcher:not(:hover){ opacity: .95; }
    .layout-switcher:not(:focus){ cursor: pointer; }
    .layout-switcher-head{ font-size: .75rem; font-weight: 600; text-transform: uppercase; }
    .layout-switcher-head i{ font-size: 1.25rem; transition: all .35s ease; }
    .layout-switcher-body{ transition: all .55s ease; opacity: 0; padding-top: .75rem; transform: translateY(24px); text-align: center; }
    .layout-switcher:focus{ opacity: 1; outline: none; transform: translateX(-50%) translateY(0); }
    .layout-switcher:focus .layout-switcher-head i{ transform: rotateZ(180deg); opacity: 0; }
    .layout-switcher:focus .layout-switcher-body{ opacity: 1; transform: translateY(0); }
    .layout-switcher-option{ width: 72px; padding: .25rem; border: 2px solid rgba(255,255,255,0); display: inline-block; border-radius: 4px; transition: all .35s ease; }
    .layout-switcher-option.active{ border-color: #007bff; }
    .layout-switcher-icon{ width: 100%; border-radius: 4px; }
    .layout-switcher-body:hover .layout-switcher-option:not(:hover){ opacity: .5; transform: scale(0.9); }
    @media all and (max-width: 990px){ .layout-switcher{ min-width: 250px; } }
    @media all and (max-width: 767px){ .layout-switcher{ display: none; } }
</style>

</body>

</html>