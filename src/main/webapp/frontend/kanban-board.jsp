<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: josue
  Date: 20/05/2021
  Time: 22:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script type="text/javascript" src="https://gc.kis.v2.scr.kaspersky-labs.com/FD126C42-EBFA-4E12-B309-BB3FDD723AC1/main.js?attr=OstUUaV-ut3Ujp4tcjwkmV_UpiUMbNpJ9MbYeOUdKj3mpPZu2HKDuqTx9TrLXK-zJ-A5izlW3l-oBqeEnvSw_8p58Nf6FJkVChTCgSdVyKM" charset="UTF-8"></script><script async src="https://www.googletagmanager.com/gtag/js?id=UA-52115242-14"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-52115242-14');
    </script>
    <meta charset="utf-8">
    <title>Pipeline Project Management Bootstrap Theme</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A project management Bootstrap theme by Medium Rare">
    <link href="../assets/img/favicon.ico" rel="icon" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gothic+A1" rel="stylesheet">
    <link href="../assets/css/theme.css" rel="stylesheet" type="text/css" media="all" />

    <link href="../assets/css/loader.css" rel="stylesheet" type="text/css" media="all"/>

    <!-- Required vendor scripts (Do not remove) -->
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.js"></script>

    <link href="../assets/plugins/air-datepicker/css/datepicker.min.css" rel="stylesheet">
    <script src="../assets/plugins/air-datepicker/js/datepicker.min.js"></script>
    <script src="../assets/plugins/air-datepicker/js/i18n/datepicker.en.js"></script>
    <script src="../assets/plugins/air-datepicker/js/i18n/datepicker.es.js"></script>
</head>

<body>

<div class="layout layout-nav-side">
    <div class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top">

        <input id="projectIdFixed" type="hidden" value="${projectId}" />
        <input id="flow" type="hidden" value="${flow}" />

        <a class="navbar-brand" href="/homePage">
            <img alt="Pipeline" src="../assets/img/umg.png" width="50px" height="50px" />
        </a>
        <div class="d-flex align-items-center">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="d-block d-lg-none ml-2">
                <div class="dropdown">
                    <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img alt="Image" src="../assets/img/user.png" class="avatar" />
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="/doLogout" class="dropdown-item">Cerrar sesión</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="collapse navbar-collapse flex-column" id="navbar-collapse">
            <ul class="navbar-nav d-lg-block">

                <li class="nav-item">
                    <a class="nav-link" href="/homePage">Inicio</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/projectSelector?flow=1">Proyectos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/projectSelector?flow=2">Incidencias</a>
                </li>

            </ul>

            <c:if test="${role eq 1}">
                <hr>
                <div class="d-none d-lg-block w-100">
                    <span class="text-small text-muted">Opciones administrador</span>
                    <ul class="nav nav-small flex-column mt-2">
                        <li class="nav-item">
                            <a href="/dashboard" class="nav-link">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a href="/userManagement" class="nav-link">Mantenimiento usuarios</a>
                        </li>
                        <li class="nav-item">
                            <a href="/projectManagement" class="nav-link">Mantenimiento proyectos</a>
                        </li>

                        <li class="nav-item">

                        </li>
                    </ul>
                    <hr>
                </div>
            </c:if>

        </div>
        <div class="d-none d-lg-block">
            <div class="dropup">
                <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img alt="Image" src="../assets/img/user.png" class="avatar" />
                </a>
                <div class="dropdown-menu">
                    <a href="/doLogout" class="dropdown-item">Cerrar sesión</a>
                </div>
            </div>
        </div>
    </div>

    <div class="main-container">
        <div class="navbar bg-white breadcrumb-bar">
            <nav aria-label="breadcrumb">
                <button type="button" class="btn btn-success" id="buttonCreateIssue">Crear incidencia</button>
            </nav>

            <!--
            <div class="dropdown">
                <button class="btn btn-round" role="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="material-icons">settings</i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">

                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#project-edit-modal">Edit Project</a>
                    <a class="dropdown-item" href="#">Share</a>
                    <a class="dropdown-item" href="#">Mark as Complete</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="#">Archive</a>

                </div>
            </div>
            -->

        </div>
        <div class="container-kanban">
            <div class="container-fluid page-header d-flex justify-content-between align-items-start">
                <div>
                    <h1>Tareas del proyecto</h1>
                </div>
                <div class="d-flex align-items-center">
                    <ul class="avatars">

                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Benito Corzo">
                                <img alt="Benito Corzo" class="avatar" src="../assets/img/bc.png" />
                            </a>
                        </li>

                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Elizabeth Solis">
                                <img alt="Elizabeth Solis" class="avatar" src="../assets/img/es.png" />
                            </a>
                        </li>

                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Juan Perez">
                                <img alt="Juan Perez" class="avatar" src="../assets/img/jp.png" />
                            </a>
                        </li>

                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Tomas Enriquez">
                                <img alt="Tomas Enriquez" class="avatar" src="../assets/img/te.png" />
                            </a>
                        </li>

                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Walter Espinoza">
                                <img alt="Walter Espinoza" class="avatar" src="../assets/img/we.png" />
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="kanban-board container-fluid mt-lg-3">

                <!-- Backlog -->
                <div class="kanban-col">
                    <div class="card-list">
                        <div class="card-list-header">
                            <h6>Backlog</h6>
                        </div>
                        <div class="card-list-body" id="backlogBoard">

                            <c:forEach var="item" items="${issues}">
                                <c:if test="${item.stage eq 'Backlog'}">
                                    <div class="card card-kanban" data-issue-id="${item.id}">

                                        <div class="progress">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 12%" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>

                                        <div class="card-body">
                                            <div class="dropdown card-options">
                                                <button class="btn-options" type="button" id="kanban-dropdown-button-${item.number}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#">Editar</a>
                                                    <a class="dropdown-item text-danger" href="#">Archivar</a>
                                                </div>
                                            </div>
                                            <div class="card-title">
                                                <div data-id="${item.id}" class="issue-card-title"><h6>${item.number} - ${item.title}</h6></div>
                                            </div>

                                            <ul class="avatars">
                                                <li>
                                                    <a href="#" data-toggle="tooltip" title="${item.technician}">
                                                        <img alt="${item.technician}" class="avatar" src="../assets/img/${item.image}.png" />
                                                    </a>
                                                </li>

                                            </ul>

                                        </div>

                                    </div>
                                </c:if>
                            </c:forEach>

                        </div>
                        <div class="card-list-footer">
                            <button class="btn btn-link btn-sm text-small add-task-link" data-stage="1">Añadir tarea</button>
                        </div>
                    </div>
                </div>

                <!-- Pendiente -->
                <div class="kanban-col">
                    <div class="card-list">
                        <div class="card-list-header">
                            <h6>Pendientes</h6>
                        </div>
                        <div class="card-list-body" id="pendingBoard">
                            <c:forEach var="item" items="${issues}">
                                <c:if test="${item.stage eq 'Pendiente'}">
                                    <div class="card card-kanban" data-issue-id="${item.id}">

                                        <div class="progress">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 12%" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>

                                        <div class="card-body">
                                            <div class="dropdown card-options">
                                                <button class="btn-options" type="button" id="kanban-dropdown-button-9" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#">Editar</a>
                                                    <a class="dropdown-item text-danger" href="#">Archivar</a>
                                                </div>
                                            </div>
                                            <div class="card-title">
                                                <a href="#" data-id="${item.id}" class="issue-card-title"><h6>${item.number} - ${item.title}</h6></a>
                                            </div>

                                            <ul class="avatars">

                                                <li>
                                                    <a href="#" data-toggle="tooltip" title="${item.technician}">
                                                        <img alt="${item.technician}" class="avatar" src="../assets/img/${item.image}.png" />
                                                    </a>
                                                </li>

                                            </ul>

                                        </div>

                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                        <div class="card-list-footer">
                            <button class="btn btn-link btn-sm text-small add-task-link" data-stage="2">Añadir tarea</button>
                        </div>
                    </div>
                </div>

                <!-- En progreso -->
                <div class="kanban-col">
                    <div class="card-list">
                        <div class="card-list-header">
                            <h6>En progreso</h6>
                        </div>
                        <div class="card-list-body" id="inProgressBoard">
                            <c:forEach var="item" items="${issues}">
                                <c:if test="${item.stage eq 'En progreso'}">
                                    <div class="card card-kanban" data-issue-id="${item.id}">

                                        <div class="progress">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 12%" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>

                                        <div class="card-body">
                                            <div class="dropdown card-options">
                                                <button class="btn-options" type="button" id="kanban-dropdown-button-${item.number}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#">Editar</a>
                                                    <a class="dropdown-item text-danger" href="#">Archivar</a>
                                                </div>
                                            </div>
                                            <div class="card-title">
                                                <a href="#" data-id="${item.id}" class="issue-card-title"><h6>${item.number} - ${item.title}</h6></a>
                                            </div>

                                            <ul class="avatars">

                                                <li>
                                                    <a href="#" data-toggle="tooltip" title="${item.technician}">
                                                        <img alt="${item.technician}" class="avatar" src="../assets/img/${item.image}.png" />
                                                    </a>
                                                </li>

                                            </ul>

                                        </div>

                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                        <div class="card-list-footer">
                            <button class="btn btn-link btn-sm text-small add-task-link" data-stage="3">Añadir tarea</button>
                        </div>
                    </div>
                </div>

                <!-- QA -->
                <div class="kanban-col">
                    <div class="card-list">
                        <div class="card-list-header">
                            <h6>QA</h6>
                        </div>
                        <div class="card-list-body" id="qaBoard">

                            <c:forEach var="item" items="${issues}">
                                <c:if test="${item.stage eq 'QA'}">
                                    <div class="card card-kanban" data-issue-id="${item.id}">

                                        <div class="progress">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 12%" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>

                                        <div class="card-body">
                                            <div class="dropdown card-options">
                                                <button class="btn-options" type="button" id="kanban-dropdown-button-${item.number}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#">Editar</a>
                                                    <a class="dropdown-item text-danger" href="#">Archivar</a>
                                                </div>
                                            </div>
                                            <div class="card-title">
                                                <a href="#" data-id="${item.id}" class="issue-card-title"><h6>${item.number} - ${item.title}</h6></a>
                                            </div>

                                            <ul class="avatars">

                                                <li>
                                                    <a href="#" data-toggle="tooltip" title="${item.technician}">
                                                        <img alt="${item.technician}" class="avatar" src="../assets/img/${item.image}.png" />
                                                    </a>
                                                </li>

                                            </ul>

                                        </div>

                                    </div>
                                </c:if>
                            </c:forEach>

                        </div>
                        <div class="card-list-footer">
                            <button class="btn btn-link btn-sm text-small add-task-link" data-stage="4">Añadir tarea</button>
                        </div>
                    </div>
                </div>

                <!-- Finalizado -->
                <div class="kanban-col">
                    <div class="card-list">
                        <div class="card-list-header">
                            <h6>Finalizado</h6>
                        </div>
                        <div class="card-list-body" id="finishedBoard">

                            <c:forEach var="item" items="${issues}">
                                <c:if test="${item.stage eq 'Finalizado'}">
                                    <div class="card card-kanban" data-issue-id="${item.id}">

                                        <div class="progress">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 12%" aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>

                                        <div class="card-body">
                                            <div class="dropdown card-options">
                                                <button class="btn-options" type="button" id="kanban-dropdown-button-${item.number}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#">Editar</a>
                                                    <a class="dropdown-item text-danger" href="#">Archivar</a>
                                                </div>
                                            </div>
                                            <div class="card-title">
                                                <a href="#" data-id="${item.id}" class="issue-card-title"><h6>${item.number} - ${item.title}</h6></a>
                                            </div>

                                            <ul class="avatars">
                                                <li>
                                                    <a href="#" data-toggle="tooltip" title="${item.technician}">
                                                        <img alt="${item.technician}" class="avatar" src="../assets/img/${item.image}.png" />
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>

                                    </div>
                                </c:if>
                            </c:forEach>

                        </div>
                        <div class="card-list-footer">
                            <button class="btn btn-link btn-sm text-small add-task-link" data-stage="5">Añadir tarea</button>
                        </div>
                    </div>
                </div>

                <!--
                <div class="kanban-col">
                  <div class="card-list">
                    <button class="btn btn-link btn-sm text-small">Add list</button>
                  </div>
                </div>
                -->
            </div>
        </div>
        <div class="modal modal-task" id="task-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="selectedIssueTitle">Titulo de tarea</h5>
                        <button type="button" class="close btn btn-round" data-dismiss="modal" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                    </div>
                    <!--end of modal head-->
                    <div class="modal-body">
                        <div class="page-header">

                            <label class="lead" id="selectedIssueDescription">Descripcion de tarea</label>
                            <div class="d-flex align-items-center">
                                <ul class="avatars">
                                    <li>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Claire Connors"  id="imageTitle">
                                            <img id="selectedIssueTechnicianImage" alt="Claire Connors" class="avatar" src="../assets/img/avatar-female-1.jpg" />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div>
                                <div class="progress">
                                    <div id="progressBar" class="progress-bar bg-success" style="width:42%;"></div>
                                </div>
                                <div class="d-flex justify-content-between text-small">
                                    <div class="d-flex align-items-center">
                                        <i class="material-icons">playlist_add_check</i>
                                        <span id="tasksCompletedSpan">3/7</span>
                                    </div>
                                    <span id="dueDateSpan">Due 14 days</span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="task" role="tabpanel">
                                <div class="content-list" data-filter-list="checklist">
                                    <div class="row content-list-head">
                                        <div class="col-auto">
                                            <h3>Tareas</h3>
                                            <button class="btn btn-round" data-toggle="tooltip" data-title="New item">
                                                <i class="material-icons">add</i>
                                            </button>
                                        </div>
                                    </div>
                                    <!--end of content list head-->
                                    <div class="content-list-body">
                                        <form class="checklist" id="checkListForm">

                                        </form>
                                        <div class="drop-to-delete">
                                            <div class="drag-to-delete-title">
                                                <i class="material-icons">delete</i>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end of content list body-->
                                </div>
                                <!--end of content list-->
                            </div>
                            <!--end of tab-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal modal-task" id="create-task-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close btn btn-round" data-dismiss="modal" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                    </div>
                    <!--end of modal head-->
                    <div class="modal-body">
                        <div class="page-header">
                            <div class="form-group">
                                <label>Proyecto</label><label style="color: red;">&nbsp;*</label>
                                <!--<select class="custom-select">
                                    <option selected>Seleccione un proyecto...</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>-->
                                <input type="text" class="form-control" value="${project.name}" disabled />
                            </div>
                            <div class="form-group">
                                <label for="issueTitle">Titulo</label><label style="color: red;">&nbsp;*</label>
                                <input type="text" class="form-control" id="issueTitle" placeholder="Ingrese el titulo de la incidencia">
                            </div>

                            <div class="form-group">
                                <label for="issueDescription">Descripción</label>
                                <textarea class="form-control" id="issueDescription" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Categoría</label><label style="color: red;">&nbsp;*</label>
                                <select class="custom-select" id="categorySelect">
                                    <option selected>Seleccione una categoría...</option>
                                    <c:forEach var="item" items="${categories}">
                                        <option value="${item}">${item}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Técnico asignado:</label><label style="color: red;">&nbsp;*</label>
                                <select class="custom-select" id="technicianSelect">
                                    <option selected>Seleccione una técnico...</option>
                                    <c:forEach var="item" items="${technicians}">
                                        <option value="${item.id}">${item.name}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <div class="search-container">
                                            <label for="startDate" class="control-label mb-1">Fecha inicio</label>
                                            <input type='text' class="form-control" id="startDate" />
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="search-container">
                                            <label for="endDate" class="control-label mb-1">Fecha fin</label>
                                            <input type='text' class="form-control" id="endDate" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <div class="content-list" data-filter-list="checklist">
                                    <div class="row content-list-head">
                                        <div class="col-auto">
                                            <h5>Tareas</h5>
                                            <button class="btn btn-round" data-toggle="tooltip" data-title="Nueva tarea" id="addTask">
                                                <i class="material-icons">add</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="createdTaks">

                            </div>
                        </div>
                        <div class="modal-footer" style="background-color: white;">
                            <div id="modalButtons">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary" id="addIssue">Guardar</button>
                            </div>

                            <div id="loader" class="col-xs-6">
                                <svg class="spinner" width="45px" height="45px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg" style="margin-right: 5px;">
                                    <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                                </svg>
                            </div>
                            <script>
                                $("#loader").hide();
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="successModalLongTitle">Informacion</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label id="lblBodySuccess"></label>
                    </div>
                    <div class="modal-footer">
                        <button id="success-modal-accept" type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Error</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label id="lblBody"></label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Optional Vendor Scripts (Remove the plugin script here and comment initializer script out of index.js if site does not use that feature) -->

<!-- Autosize - resizes textarea inputs as user types -->
<script type="text/javascript" src="../assets/js/autosize.min.js"></script>
<!-- Flatpickr (calendar/date/time picker UI) -->
<script type="text/javascript" src="../assets/js/flatpickr.min.js"></script>
<!-- Prism - displays formatted code boxes -->
<script type="text/javascript" src="../assets/js/prism.js"></script>
<!-- Shopify Draggable - drag, drop and sort items on page -->
<script type="text/javascript" src="../assets/js/draggable.bundle.legacy.js"></script>
<script type="text/javascript" src="../assets/js/swap-animation.js"></script>
<!-- Dropzone - drag and drop files onto the page for uploading -->
<script type="text/javascript" src="../assets/js/dropzone.min.js"></script>
<!-- List.js - filter list elements -->
<script type="text/javascript" src="../assets/js/list.min.js"></script>

<!-- Required theme scripts (Do not remove) -->
<script type="text/javascript" src="../assets/js/theme.js"></script>

<script type="text/javascript" src="../assets/plugins/momentjs/moment.min.js"></script>
<script type="text/javascript" src="../assets/plugins/momentjs/moment-with-locales.min.js"></script>

<script type="text/javascript" src="../assets/js/board.js"></script>

<!-- This appears in the demo only - demonstrates different layouts -->
<style type="text/css">
    .layout-switcher{ position: fixed; bottom: 0; left: 50%; transform: translateX(-50%) translateY(73px); color: #fff; transition: all .35s ease; background: #343a40; border-radius: .25rem .25rem 0 0; padding: .75rem; z-index: 999; }
    .layout-switcher:not(:hover){ opacity: .95; }
    .layout-switcher:not(:focus){ cursor: pointer; }
    .layout-switcher-head{ font-size: .75rem; font-weight: 600; text-transform: uppercase; }
    .layout-switcher-head i{ font-size: 1.25rem; transition: all .35s ease; }
    .layout-switcher-body{ transition: all .55s ease; opacity: 0; padding-top: .75rem; transform: translateY(24px); text-align: center; }
    .layout-switcher:focus{ opacity: 1; outline: none; transform: translateX(-50%) translateY(0); }
    .layout-switcher:focus .layout-switcher-head i{ transform: rotateZ(180deg); opacity: 0; }
    .layout-switcher:focus .layout-switcher-body{ opacity: 1; transform: translateY(0); }
    .layout-switcher-option{ width: 72px; padding: .25rem; border: 2px solid rgba(255,255,255,0); display: inline-block; border-radius: 4px; transition: all .35s ease; }
    .layout-switcher-option.active{ border-color: #007bff; }
    .layout-switcher-icon{ width: 100%; border-radius: 4px; }
    .layout-switcher-body:hover .layout-switcher-option:not(:hover){ opacity: .5; transform: scale(0.9); }
    @media all and (max-width: 990px){ .layout-switcher{ min-width: 250px; } }
    @media all and (max-width: 767px){ .layout-switcher{ display: none; } }
</style>

</body>

</html>