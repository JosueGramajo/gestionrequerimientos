<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: promerica
  Date: 20/05/21
  Time: 13:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script type="text/javascript" src="https://gc.kis.v2.scr.kaspersky-labs.com/FD126C42-EBFA-4E12-B309-BB3FDD723AC1/main.js?attr=dCkwFMcQjS8iQ4jhO8e5FDHWy7s_Opr7Z4lkJ4Dqwo4-wJ03VktR4AJ4E_5GTS448LBEtYWORvA-oMrOS5nwTA" charset="UTF-8"></script><script async src="https://www.googletagmanager.com/gtag/js?id=UA-52115242-14"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-52115242-14');
    </script>
    <meta charset="utf-8">
    <title>Gestor requerimientos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A project management Bootstrap theme by Medium Rare">
    <link href="../assets/img/favicon.ico" rel="icon" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gothic+A1" rel="stylesheet">
    <link href="../assets/css/theme.css" rel="stylesheet" type="text/css" media="all" />
</head>

<body>

<div class="layout layout-nav-side">
    <div class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top">

        <a class="navbar-brand" href="/homePage">
            <img alt="Pipeline" src="../assets/img/umg.png" width="50px" height="50px" />
        </a>
        <div class="d-flex align-items-center">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="d-block d-lg-none ml-2">
                <div class="dropdown">
                    <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img alt="Image" src="../assets/img/user.png" class="avatar" />
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="/doLogout" class="dropdown-item">Cerrar sesión</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="collapse navbar-collapse flex-column" id="navbar-collapse">
            <ul class="navbar-nav d-lg-block">

                <li class="nav-item">
                    <a class="nav-link" href="/homePage">Inicio</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/projectSelector?flow=1">Proyectos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/projectSelector?flow=2">Incidencias</a>
                </li>

            </ul>

            <c:if test="${role eq 1}">
                <hr>
                <div class="d-none d-lg-block w-100">
                    <span class="text-small text-muted">Opciones administrador</span>
                    <ul class="nav nav-small flex-column mt-2">
                        <li class="nav-item">
                            <a href="/dashboard" class="nav-link">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a href="/userManagement" class="nav-link">Mantenimiento usuarios</a>
                        </li>
                        <li class="nav-item">
                            <a href="/projectManagement" class="nav-link">Mantenimiento proyectos</a>
                        </li>

                        <li class="nav-item">

                        </li>
                    </ul>
                    <hr>
                </div>
            </c:if>

        </div>
        <div class="d-none d-lg-block">
            <div class="dropup">
                <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img alt="Image" src="../assets/img/user.png" class="avatar" />
                </a>
                <div class="dropdown-menu">
                    <a href="/doLogout" class="dropdown-item">Cerrar sesión</a>
                </div>
            </div>
        </div>
    </div>

    <div class="main-container">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-11 col-xl-9">
                    <section class="py-4 py-lg-5">
                        <div class="mb-3 d-flex">
                            <img alt="Pipeline" src="../assets/img/umg.png" class="avatar avatar-lg mr-1" />
                            <div>
                                <span class="badge badge-success">1.3.0</span>
                            </div>
                        </div>
                        <h1 class="display-4 mb-3">Sistema para Gestión de Requerimientos y Proyectos.</h1>
                        <p class="lead">
                            Dentro de la organización de Gestión de la Tecnología S.A. se ha crecido en un 50% en los últimos tres años, los cual ha demandado la cantidad de personal para soportar la operación de la organización. Uno de los departamentos que tienes un incremento en su carga laborales es el departamento de tecnología.
                        </p>
                        <p class="lead">
                            El gerente de IT ha solicitado constantemente incremento en el número del personal asignado en la organización, pero la respuesta ha sido siempre que el personal asignado no ocupa el tiempo reportado en sus actividades.
                        </p>
                        <p class="lead">
                            Por tal razón, se ha lanzado un proyecto para el diseño e implementación de un sistema de software en el cual se pueda registrar y llevar el seguimiento de los requerimientos y proyectos asignados a los 5 técnicos del departamento.
                        </p>
                    </section>
                    <section class="py-4 py-lg-5">
                        <div class="d-flex align-items-center mb-3">
                            <i class="material-icons text-primary mr-3 h1">supervised_user_circle</i>
                            <h2 class="h1 mb-0">Equipo de trabajo</h2>
                        </div>
                        <p class="lead">
                            El análisis y desarrollo de este software fué realizado por el siguiente equipo.
                        </p>

                        <table class="table">
                            <tbody>
                            <tr>
                                <td>Ana Kristina Cifuentes Castañeda</td>
                                <td>0900-16-14277</td>
                            </tr>
                            <tr>
                                <td>Manuel de Jesús Vega Gómez</td>
                                <td>0900-15-2718</td>
                            </tr>
                            <tr>
                                <td>Berner Aldahir Raquec Tzay</td>
                                <td>0900-15-17017</td>
                            </tr>
                            <tr>
                                <td>Brandon Gerardo Sicajá Arrivillaga</td>
                                <td>0900-14-3459</td>
                            </tr>
                            <tr>
                                <td>Josué Alejandro Gramajo Cujantes</td>
                                <td>0900-16-3067</td>
                            </tr>
                            </tbody>
                        </table>
                    </section>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Required vendor scripts (Do not remove) -->
<script type="text/javascript" src="../assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../assets/js/popper.min.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap.js"></script>

<!-- Optional Vendor Scripts (Remove the plugin script here and comment initializer script out of index.js if site does not use that feature) -->

<!-- Autosize - resizes textarea inputs as user types -->
<script type="text/javascript" src="../assets/js/autosize.min.js"></script>
<!-- Flatpickr (calendar/date/time picker UI) -->
<script type="text/javascript" src="../assets/js/flatpickr.min.js"></script>
<!-- Prism - displays formatted code boxes -->
<script type="text/javascript" src="../assets/js/prism.js"></script>
<!-- Shopify Draggable - drag, drop and sort items on page -->
<script type="text/javascript" src="../assets/js/draggable.bundle.legacy.js"></script>
<script type="text/javascript" src="../assets/js/swap-animation.js"></script>
<!-- Dropzone - drag and drop files onto the page for uploading -->
<script type="text/javascript" src="../assets/js/dropzone.min.js"></script>
<!-- List.js - filter list elements -->
<script type="text/javascript" src="../assets/js/list.min.js"></script>

<!-- Required theme scripts (Do not remove) -->
<script type="text/javascript" src="../assets/js/theme.js"></script>

<!-- This appears in the demo only - demonstrates different layouts -->
<style type="text/css">
    .layout-switcher{ position: fixed; bottom: 0; left: 50%; transform: translateX(-50%) translateY(73px); color: #fff; transition: all .35s ease; background: #343a40; border-radius: .25rem .25rem 0 0; padding: .75rem; z-index: 999; }
    .layout-switcher:not(:hover){ opacity: .95; }
    .layout-switcher:not(:focus){ cursor: pointer; }
    .layout-switcher-head{ font-size: .75rem; font-weight: 600; text-transform: uppercase; }
    .layout-switcher-head i{ font-size: 1.25rem; transition: all .35s ease; }
    .layout-switcher-body{ transition: all .55s ease; opacity: 0; padding-top: .75rem; transform: translateY(24px); text-align: center; }
    .layout-switcher:focus{ opacity: 1; outline: none; transform: translateX(-50%) translateY(0); }
    .layout-switcher:focus .layout-switcher-head i{ transform: rotateZ(180deg); opacity: 0; }
    .layout-switcher:focus .layout-switcher-body{ opacity: 1; transform: translateY(0); }
    .layout-switcher-option{ width: 72px; padding: .25rem; border: 2px solid rgba(255,255,255,0); display: inline-block; border-radius: 4px; transition: all .35s ease; }
    .layout-switcher-option.active{ border-color: #007bff; }
    .layout-switcher-icon{ width: 100%; border-radius: 4px; }
    .layout-switcher-body:hover .layout-switcher-option:not(:hover){ opacity: .5; transform: scale(0.9); }
    @media all and (max-width: 990px){ .layout-switcher{ min-width: 250px; } }
    @media all and (max-width: 767px){ .layout-switcher{ display: none; } }
</style>
<div class="layout-switcher" tabindex="1">
    <div class="layout-switcher-head d-flex justify-content-between">
        <span>Select Layout</span>
        <i class="material-icons">arrow_drop_up</i>
    </div>
    <div class="layout-switcher-body">

    </div>
</div>

</body>

</html>