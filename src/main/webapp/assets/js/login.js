jQuery(document).ready(function ($) {

    var infoMessage = "";
    var error = "";

    $("#login-button").click(function () {

        $("#loginButton").hide();
        $("#loader").show();

        $.ajax({
            method: "POST",
            url: "/doLogin",
            data: {
                "email" : $("#userEmail").val(),
                "password" : $("#userPassword").val()
            },
            success: function ( data ) {
                $("#loginButton").show();
                $("#loader").hide();

                window.location.replace("/homePage")
            },
            error: function (xhr) {
                $("#loginButton").show();
                $("#loader").hide();

                error = xhr.responseText;
                $('#errorModal').modal('show');
            }
        });
    });

    $('#successModal').on('show.bs.modal', function (event) {
        let modal = $(this);
        modal.find('.modal-body label').html(infoMessage);
    });

    $('#errorModal').on('show.bs.modal', function (event) {
        let modal = $(this);
        modal.find('.modal-body label').html(error);
    });
})