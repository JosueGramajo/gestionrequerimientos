jQuery(document).ready(function ($) {

    var reaload = false;

    var currentId = ""

    var loadedObject = null;

    var isUpdate = false;

    let today = new Date();
    let fiveDaysPrior = new Date();
    fiveDaysPrior.setDate(fiveDaysPrior.getDate() - 5);
    let dp = $("#startDate").datepicker({
        language: 'es',
        autoClose: true,
        startDate: today,
        minDate: today
    }).data('datepicker');

    $("#endDate").datepicker({
        language: 'es',
        autoClose: true,
        startDate: today,
        minDate: today
    });
    dp.selectDate(today);

    $("#example1").on('click','.edit-button', function () {
        let id = $(this).attr("data-id");
        currentId = id

        $.ajax({
            method: "POST",
            url: "/getProjectWithId",
            data: { "id": id },
            success: function (data) {
                loadedObject = $.parseJSON(data)

                $("#insertProjectModal").modal('show');
            },
            error: function (xhr) {

            }
        });
    }).on('click', '.delete-button', function () {
        currentId = $(this).attr("data-id");

        $("#confirmationModal").modal('show');
    });

    $("#confirmationCancel").click(function () {
        currentId = "";
    });

    $("#confirmationAccept").click(function () {
        $.ajax({
            url: "/deleteProject",
            method: "DELETE",
            data: {
                "id" : currentId
            },
            success: function (data) {
                $("#confirmationModal").modal('hide');

                reload = true;

                $("#infoModalMessage").html("Proyecto eliminado exitosamente");
                $("#infoModal").modal('show');
            },
            error: function (xhr) {
                $("#confirmationModal").modal('hide');

                $("#infoModalMessage").html(xhr.responseText);
                $("#infoModal").modal('show');
            }
        });
    });

    $("#confirmationModal").on('show.bs.modal', function () {
        $("#confirmationText").html("ESTA TOTALMENTE SEGURO QUE DESEA ELIMINAR ESTE PROYECTO? Esta accion no puede repetirse");
    });

    $("#infoAccept").click(function () {
        if (reload){
            window.location.reload();
        }
    });

    $("#addProjectButton").click(function () {
        $("#insertProjectModal").modal('show');
    });

    $("#insertProjectModal").on('show.bs.modal', function () {
        if (loadedObject != null){
            $("#projectName").val(loadedObject.name);
            $("#projectDescription").val(loadedObject.description);
            $("#projectPerson option[value=\""+ loadedObject.personInCharge +"\"]").attr("selected", "selected");
            $("#projectCost").val(loadedObject.cost);
            isUpdate = true;
        }
    })

    $("#saveProjectButton").click(function () {
        if (isUpdate){
            isUpdate = false;

            $.ajax({
                method: "PUT",
                url: "/updateProject",
                data: {
                    "id": currentId,
                    "name": $("#projectName").val(),
                    "description": $("#projectDescription").val(),
                    "personInCharge": $("#projectPerson").val(),
                    "startDate": $("#startDate").val(),
                    "estimatedEndDate": $("#endDate").val(),
                    "cost": $("#projectCost").val()
                },
                success: function (data) {
                    $("#insertProjectModal").modal('hide');

                    reload = true;

                    $("#infoModalMessage").html("Proyecto actualizado exitosamente");
                    $("#infoModal").modal('show');
                },
                error: function (xhr) {
                    $("#insertProjectModal").modal('hide');

                    $("#infoModalMessage").html(xhr.responseText);
                    $("#infoModal").modal('show');
                }
            });
        }else{
            $.ajax({
                method: "POST",
                url: "/addProject",
                data: {
                    "name": $("#projectName").val(),
                    "description": $("#projectDescription").val(),
                    "personInCharge": $("#projectPerson").val(),
                    "startDate": $("#startDate").val(),
                    "estimatedEndDate": $("#endDate").val(),
                    "cost": $("#projectCost").val()
                },
                success: function (data) {
                    $("#insertProjectModal").modal('hide');

                    reload = true;

                    $("#infoModalMessage").html("Proyecto agregado exitosamente");
                    $("#infoModal").modal('show');
                },
                error: function (xhr) {
                    $("#insertProjectModal").modal('hide');

                    $("#infoModalMessage").html(xhr.responseText);
                    $("#infoModal").modal('show');
                }
            });
        }

    });
});