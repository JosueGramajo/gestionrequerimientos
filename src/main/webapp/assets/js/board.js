jQuery(document).ready(function($){

    var success = ""

    var error = ""

    var selectedIssue;

    var state = 1;

    var currentCompletedTasks = 0;

    $("#buttonCreateIssue").click(function(){
        state = 1;
        $("#create-task-modal").modal('show');
    });

    $(".add-task-link").click(function () {
        state = $(this).attr("data-stage");
        $("#create-task-modal").modal('show');
    })

    let today = new Date();
    let fiveDaysPrior = new Date();
    fiveDaysPrior.setDate(fiveDaysPrior.getDate() - 5);
    let dp = $("#startDate").datepicker({
        language: 'es',
        autoClose: true,
        startDate: today,
        minDate: today
    }).data('datepicker');

    $("#endDate").datepicker({
        language: 'es',
        autoClose: true,
        startDate: today,
        minDate: today
    });
    dp.selectDate(today);
    
    $(".issue-card-title").on('click', function (e) {
        $.ajax({
            method: "POST",
            url: "/getIssue",
            data: {
                "id" : $(this).data('id')
            },
            success: function ( data ) {
                selectedIssue = JSON.parse(data);

                $("#task-modal").modal('show');
            },
            error: function (xhr) {
                error = xhr.responseText;
                $('#errorModal').modal('show');
            }
        });
    });

    $('#task-modal').on('show.bs.modal', function (event) {
        console.log(selectedIssue);

        $("#selectedIssueTitle").html(selectedIssue.title);
        $("#selectedIssueDescription").html(selectedIssue.description);
        $("#selectedIssueTechnicianImage").attr("src", "../assets/img/"+ selectedIssue.image + ".png");
        $("#imageTitle").attr("title", selectedIssue.technician);

        let checkListForm = $("#checkListForm");
        checkListForm.empty();

        var completedTasks = 0

        $.each( selectedIssue.tasks, function( i, value ) {
            var checked = "";
            if (value.completed){
                checked = 'checked';
                completedTasks++;
            }

            checkListForm.append('<div class="row">\n' +
                '    <div class="form-group col">\n' +
                '          <span class="checklist-reorder">\n' +
                '            <i class="material-icons">reorder</i>\n' +
                '          </span>\n' +
                '        <div class="custom-control custom-checkbox col">\n' +
                '            <input type="checkbox" class="custom-control-input" name="check-tasks" id="checklist-item-'+ (i + 1) +'" data-prev-checked="'+ value.completed +'" data-issue-id="' + selectedIssue.id + '" data-task-id="'+ value.id +'" '+checked+'>\n' +
                '            <label class="custom-control-label" for="checklist-item-'+ (i + 1) +'"></label>\n' +
                '            <div class="col">\n' +
                '                <input class="form-control" type="text" value="'+ value.description +'" />\n' +
                '                <div class="checklist-strikethrough"></div>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>');

            currentCompletedTasks = completedTasks;
            $("#tasksCompletedSpan").html(completedTasks + "/" + selectedIssue.tasks.length);

            let percentageCompeleted = Math.ceil(((completedTasks * 100) / selectedIssue.tasks.length));
            $("#progressBar").css('width', percentageCompeleted + '%');

            let endDate = moment(selectedIssue.estimatedEndDate, "DD-MM-YYYY").toDate();
            let startDate = new Date();
            let diff = new Date(endDate - startDate);
            var days = Math.ceil(diff/1000/60/60/24);

            var messageDates = "";
            if (days < 0){
                messageDates = "Esta incidencia ya termino!";
            }else if (days === 0){
                messageDates = "Esta incidencia termina hoy";
            }else{
                messageDates = "Termina en " + days + " días.";
            }

            $("#dueDateSpan").html(messageDates);
        });
    });

    $(document).on("change", "input[name=check-tasks]", function () {
        let checked = this.checked;
        let taskId = $(this).attr("data-task-id");
        let issueId = $(this).attr("data-issue-id");
        let prevChecked = $(this).attr("data-prev-checked");

        if (checked !== prevChecked){
            $.ajax({
                method: "POST",
                url: "/updateTaskState",
                data: {
                    "issueId" : issueId,
                    "taskId" : taskId,
                    "completed" : checked
                },
                success: function ( data ) {
                    if (checked){
                        currentCompletedTasks++;
                    }else{
                        currentCompletedTasks--;
                    }

                    $("#tasksCompletedSpan").html(currentCompletedTasks + "/" + selectedIssue.tasks.length);

                    let percentageCompeleted = Math.ceil(((currentCompletedTasks * 100) / selectedIssue.tasks.length));
                    $("#progressBar").css('width', percentageCompeleted + '%');
                },
                error: function (xhr) {
                    error = xhr.responseText;
                    $('#errorModal').modal('show');
                }
            });
        }
    });

    $("#addIssue").click(function () {
        $("#modalButtons").hide();
        $("#loader").show();

        let tasks = "";
        $("#createdTaks").children("input").each(function(){
            tasks = tasks + this.value + "|";
        });

        $.ajax({
            method: "POST",
            url: "/insertIssue",
            data: {
                "projectId" : $("#projectIdFixed").val(),
                "title" : $("#issueTitle").val(),
                "description" : $('textarea#issueDescription').val(),
                "category" : $("#categorySelect").val(),
                "technician" : $("#technicianSelect option:selected").text(),
                "technicianId" : $("#technicianSelect").val(),
                "startDate" : $("#startDate").val(),
                "endDate" : $("#endDate").val(),
                "tasks": tasks,
                "stage" : state
            },
            success: function ( data ) {
                $("#modalButtons").show();
                $("#loader").hide();
                $("#create-task-modal").modal('hide');

                window.location.reload();
            },
            error: function (xhr) {
                $("#modalButtons").show();
                $("#loader").hide();

                error = xhr.responseText;
                $('#errorModal').modal('show');
            }
        });
    });

    $("#addTask").click(function(){
        $("#createdTaks").append('<input class="form-control" type="text" /> <br>');
    });

    $('body').on('mousedown', '.card-kanban', function(event) {
        let $el = $(this);

        $('body').one('mouseup', function() { //revert back on the next mouse up only, wherever that mouseup takes place
            window.setTimeout(function(){
                let issueId = $el.attr("data-issue-id");
                let parentId = $( "div[data-issue-id=" + issueId + "]" ).parent().attr('id');

                var state = 0;
                switch (parentId) {
                    case 'backlogBoard':
                        state = 1;
                        break;
                    case 'pendingBoard':
                        state = 2;
                        break;
                    case 'inProgressBoard':
                        state = 3;
                        break;
                    case 'qaBoard':
                        state = 4;
                        break;
                    case 'finishedBoard':
                        state = 5;
                        break;
                }

                $.ajax({
                    method: "POST",
                    url: "/updateIssueStage",
                    data: {
                        "issueId" : issueId,
                        "state" : state
                    }
                });
            }, 600);
        });
    });


});