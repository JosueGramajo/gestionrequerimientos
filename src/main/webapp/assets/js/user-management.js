jQuery(document).ready(function ($) {

    var reaload = false;

    var currentId = ""

    var loadedObject = null;

    var isUpdate = false;

    $("#example1").on('click','.edit-button', function () {
        let id = $(this).attr("data-id");
        currentId = id

        $.ajax({
            method: "POST",
            url: "/getUserWithId",
            data: { "id": id },
            success: function (data) {
                loadedObject = $.parseJSON(data)

                $("#insertUserModal").modal('show');
            },
            error: function (xhr) {

            }
        });
    }).on('click', '.delete-button', function () {
        currentId = $(this).attr("data-id");

        $("#confirmationModal").modal('show');
    });

    $("#confirmationCancel").click(function () {
        currentId = "";
    });

    $("#confirmationAccept").click(function () {
        $.ajax({
            url: "/deleteUser",
            method: "DELETE",
            data: {
                "id" : currentId
            },
            success: function (data) {
                $("#confirmationModal").modal('hide');

                reload = true;

                $("#infoModalMessage").html("Usuario eliminado exitosamente");
                $("#infoModal").modal('show');
            },
            error: function (xhr) {
                $("#confirmationModal").modal('hide');

                $("#infoModalMessage").html(xhr.responseText);
                $("#infoModal").modal('show');
            }
        });
    });

    $("#confirmationModal").on('show.bs.modal', function () {
        $("#confirmationText").html("Esta seguro que desea eliminar a este usuario?");
    });

    $("#infoAccept").click(function () {
        if (reload){
            window.location.reload();
        }
    });

    $("#addUserButton").click(function () {
        $("#insertUserModal").modal('show');
    });

    $("#insertUserModal").on('show.bs.modal', function () {
        if (loadedObject != null){
            $("#userName").val(loadedObject.name);
            $("#userEmail").val(loadedObject.email);
            $("#userPassword").val(loadedObject.password);
            $("#userCode").val(loadedObject.employerCode);
            $("#userRole option[value=\""+ loadedObject.role +"\"]").attr("selected", "selected");
            $('#userDepartment option:contains("' + loadedObject.department + '")').attr("selected", "selected");
            isUpdate = true;
        }
    })

    $("#saveUserButton").click(function () {
        if (isUpdate){
            isUpdate = false;

            $.ajax({
                method: "PUT",
                url: "/updateUser",
                data: {
                    "id": currentId,
                    "email": $("#userEmail").val(),
                    "password": $("#userPassword").val(),
                    "name": $("#userName").val(),
                    "role": $("#userRole").val(),
                    "code": $("#userCode").val(),
                    "department": $("#userDepartment").val(),
                },
                success: function (data) {
                    $("#insertUserModal").modal('hide');

                    reload = true;

                    $("#infoModalMessage").html("Usuario actualizado exitosamente");
                    $("#infoModal").modal('show');
                },
                error: function (xhr) {
                    $("#insertUserModal").modal('hide');

                    $("#infoModalMessage").html(xhr.responseText);
                    $("#infoModal").modal('show');
                }
            });
        }else{
            $.ajax({
                method: "POST",
                url: "/insertUser",
                data: {
                    "email": $("#userEmail").val(),
                    "password": $("#userPassword").val(),
                    "name": $("#userName").val(),
                    "role": $("#userRole").val(),
                    "code": $("#userCode").val(),
                    "department": $("#userDepartment").val(),
                },
                success: function (data) {
                    $("#insertUserModal").modal('hide');

                    reload = true;

                    $("#infoModalMessage").html("Usuario agregado exitosamente");
                    $("#infoModal").modal('show');
                },
                error: function (xhr) {
                    $("#insertUserModal").modal('hide');

                    $("#infoModalMessage").html(xhr.responseText);
                    $("#infoModal").modal('show');
                }
            });
        }

    });
});